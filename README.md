Hrnek's PROTOTYPE texturepack for Doom
===

![title](pics/titlepic.png "title")

`STARTAN2` is an *interesting* choice for default wall texture. It doesn't show tiling very clearly, so that's what this texture pack is aiming to address. 

Features:
---

- 12 colour tilesets with 3 shades each, plus black and white
- Every colour is in variants of:
  - 128px no border
  - 128px border
  - 64px border
  - 32px border
- Including flats 64x64 with and without border
- Animated walls and flats
- Switches
- Midbars
- Text
  - Letters
  - Numbers
  - Symbols

![Example-techbase](pics/example1.png "Example-techbase")

current version: v0.1
[Doomworld forums development thread](https://www.doomworld.com/forum/topic/131910)
